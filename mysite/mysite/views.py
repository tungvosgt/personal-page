from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from cars.models import UsedCar

def home_page(request):
    cars = UsedCar.objects.all()
    return render(request, "home.html", {'cars' : cars})
