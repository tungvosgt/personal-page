from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.UsedCar)
admin.site.register(models.NewCar)