from django.db import models

# Create your models here.

class AbstractCar(models.Model):
    make = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    year = models.IntegerField()
    mileage = models.IntegerField()
    gearbox = models.CharField(max_length=20)
    fuel = models.CharField(max_length=30)
    old = models.BooleanField(default=True)
    price = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    photo = models.ImageField(upload_to='../assets/img', default=None)

    class Meta:
        abstract = True

    def __str__(self):
        return "{} / {} / {} / {:,}€".format(self.make, self.model, self.year, self.price)


class UsedCar(AbstractCar):
    old = True
    objects = models.Manager()

class NewCar(AbstractCar):
    old = False
    objects = models.Manager()


