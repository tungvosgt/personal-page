from django import template
from django.utils.safestring import mark_safe

from courses.models import Course

import markdown2 #remember to install markdown2 using: pip install markdown2

register = template.Library()

@register.simple_tag
def newest_course():
    ''' Get the most recent course that was added to the library. '''
    return Course.objects.latest('created_at')

@register.inclusion_tag('courses/course_nav.html')
def nav_courses_list():
    ''' return the courses list to the tag '''
    courses = Course.objects.all()
    return {'courses' : courses}

@register.filter('time_estimate')
def time_estimate(word_count):
    ''' estimate the time this course takes base on the words count '''
    minutes = round(word_count/20)
    return minutes

@register.filter('markdown_to_html')
def markdown_to_html(markdown_text):
    ''' Converts text to html '''
    html_body = markdown2.markdown(markdown_text)
    return mark_safe(html_body)









