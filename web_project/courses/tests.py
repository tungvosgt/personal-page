from django.test import TestCase
from django.utils import timezone

from .models import Course
# Create your tests here.

class CourseModelTest(TestCase):
    def test_course_creation(self):
        course= Course.objects.create(
            title='Python test',
            description='This is to test the model'
        )
        now = timezone.now()
        self.assertLessEqual(course.created_at, now)
