<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-pause="false">
<ol class="carousel-indicators">
    <?php 
    $count = 0;
    foreach ($country as $item) {
        if ($item['title'] == 'Sweden') {
            echo '<li data-target="#carouselExampleIndicators" data-slide-to="'
                . $count
                . '" class="active"></li>';
        } else {
            echo '<li data-target="#carouselExampleIndicators" data-slide-to="'
                . $count
                . '"></li>';
        }

        $count++;
    }
    ?>
</ol>
<div class="carousel-inner">
    <div class="filter-div">
        <form action="">
            <div class="filter">
                <div class="row row-title">
                    <div class="col-3 ">
                    Country
                    </div>
                    <div class="col-3 ">
                    Arrival date
                    </div>
                    <div class="col-3 ">
                    Departure date
                    </div>
                    <div class="col-3 ">
                    Number of travelers
                    </div>
                </div>
                <div class="row row-option">
                    <div class="col-3 country-select">
                        <i class="fas fa-map-marker-alt" style="margin-top:10px;"></i>
                        <select class="form-control custom-select" data-style="btn-new">       
                            <option selected>Select a country</option>
                            <option value="sweden">Sweden</option>
                            <option value="finland">Finland</option>
                            <option value="norway">Norway</option>
                            <option value="denmark">Denmark</option>
                        </select>
                    </div>
                    <div class="col-6 ">
                        <div id="arrival" class="datepick"><i class="far fa-calendar-alt"></i>  <input type="text" value="Select a date" onfocus="this.value=''"></div>
                        <div id="departure" class="datepick"><i class="far fa-calendar-alt"></i>  <input type="text" value="Select a date" onfocus="this.value=''"></div>
                    </div>
                    <div class="col-3 number-people">
                        <i class="fas fa-users"></i>
                        <input type="text" value="Type a number" onfocus="this.value=''">
                    </div>
                </div>
                <div class="row row-submit">
                    <button type="submit" class="btn discover shadow-sm mb-5" >Discover</button>
                </div>
            </div>
        </form>
    </div>
<?php 
foreach ($country as $item) {
    if ($item['title'] == 'Sweden') {
        echo '<div class="carousel-item active">
            <img class="d-block w-100" src="'
            . $item['img']
            . '" alt="First slide">
            <h1>Welcome to ' . $item['title'] . '</h1>
        </div>';
    } else {
        echo '<div class="carousel-item">
        
            <img class="d-block w-100" src="'
            . $item['img']
            . '" alt="First slide">
            <h1>Welcome to ' . $item['title'] . '</h1>
        </div>';
    }
}
?>
</div>
