<?php 

require 'connection.php';

$sql = "SELECT title, image_path FROM countries";
$result = $connection->query($sql);

$country = [];

if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $country[] = [
            "title" => $row['title'],
            "img" => $row['image_path'],
            // "thumbnail" => $row['video_thumbnail']
            // "video" => $row['video_path']
        ];
    }
} else {
    echo "0 results";
}

?>