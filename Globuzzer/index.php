<?php include('./include/data.php'); 
	require 'connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	 crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="./css/index.css">
	<link rel="icon" type="image/ico" href="https://media1-production-mightynetworks.imgix.net/asset/2164118/47_n.png?ixlib=rails-0.3.0&auto=format&w=68&h=68&fit=crop&crop=faces" />
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	 crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	 crossorigin="anonymous"></script>
	 
<script>
	function choose(country){
		var activeVideo = document.getElementById("active-video");
		var videocontainer = document.getElementById('selected-video');
		console.log(country);
		var video_src = './video/' + country + '.mp4';	
		console.log(video_src);
		videocontainer.pause();
		activeVideo.setAttribute('src', video_src);
    	videocontainer.load();
    	//videocontainer.setAttribute('poster', newposter); //Changes video poster image
    	videocontainer.play();
	}	
</script>
	<title>Globuzzer</title>
	
</head>

<body>
	
	<?php 
	// include navbar to homepage
	include('./include/navbar.php');

	// include slideshow to homepage
	include('./include/carousel.php');
	?>

	<!-- section 1 -->
	<div class="section1">
		<div class="small-container">
			<div>
			<p style="color: #425B6F; font-weight: bolder; width:40%; margin-top:12%;">Discover the Nordics<br>with our packages</p>
			</div>
			
			<div>
			<p style="color: #5e5e5e; font-weight: 400; width:60%; line-height:40px; margin-top:8%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas pulvinar velit eu nisl semper mattis. Suspendisse non vulputate nibh. Nam nec pulvinar lorem, sed blandit elit.</p>
			</div>
			
		</div>
	</div>

	<!-- section 2 -->
	<div class="section2">
		<div class="small-container">
			<div class="service">
				<div class="row title">
					<div class="col-12">
						We've got everything you need, all in one place
					</div>
				</div>
				<div class="row detail">
					<div class="col-3" onclick="window.location='#';">
						<img src="./img/telemarketer.svg" >
						<p>24/7 customer support</p>
					</div>
					<div class="col-3" onclick="window.location='#';">
						<img src="./img/map.svg" >
						<p>Local guide</p>
					</div>
					<div class="col-3" onclick="window.location='#';">
						<img src="./img/customer.svg" >
						<p>Customized packages</p>
					</div>
					<div class="col-3" onclick="window.location='#';">
						<img src="./img/list.svg" >
						<p>All in one</p>
					</div>
				</div>
			</div>	
		</div>
	</div>

	<!-- section 3 -->
	<div class="section3">
		<div class="small-container">
			<div class="row choice-title">
				<div class="col-12">
					Choose a country where you want to travel to
				</div>
			</div>
			<div class="row choice">
				<?php 

					$activate = null;

					foreach($country as $item){
						echo '<div class="col-6 p-1" >'
						. '<div name="choose-country" class="choose-div">'
						. '<h2>'. $item['title'] .'</h2>'
						. '<button value="' . $item['title'] .'" onclick="choose('. '\'' . $item['title']. '\'' . ')" class="btn choose shadow-sm mb-5">Choose</button></div>'
						. '<img src="' 
						. $item['img'] 
						. '" alt="' . $item['title'] . '">'
						. '</div>';
					}


				?>
			</div>
		</div>
	</div>

	<!-- section 4 -->

	<div class="section4">
		<div class="small-container">
			<div class="row video-title">
				<div class="col-12">
					Explore the Nordics
				</div>
			</div>
			<div class="row video-player">
				<div class="col-9">
					<video id="selected-video"  class="sampleMovie" width="100%" poster="./img/sweden-thumbnail.PNG"   preload controls>
						<source id="active-video" src="./video/Sweden.mp4" />
					</video>
				</div>

				<div class="col-3">
					<div class="col-12 small-video-player ">
						<video class="sampleMovie" width="100%" poster="./img/finland-thumbnail.PNG"  preload controls>
							<source src="./video/Finland.mp4" />
						</video>
					</div>

					<div class="col-12 small-video-player ">
						<video class="sampleMovie" width="100%" poster="./img/norway-thumbnail.PNG" preload controls >
							<source src="./video/Norway.mp4" />
						</video>
					</div>

					<div class="col-12 small-video-player ">
						<video class="sampleMovie" width="100%" poster="./img/denmark-thumbnail.PNG" preload controls >
							<source src="./video/Denmark.mp4" />
						</video>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- section 5 -->
	
	<div class="section5">
		<div class="small-container">
			<div class="row contact-title">
				<div class="col-12">
					Contact us
				</div>
			</div>
			<div class="row contact">
				<form method="" action="" class="col-7 " style="padding-left:0">
					<div class="form-input " style="height:91px;">
						<label for="name">How shall we call you? *</label>
							<input type="text" maxlength="50" name="customer-name" id="name" placeholder="Type your name here" onfocus="this.placeholder=''">
					</div>

					<div class="form-input " style="height:91px;">
						<label for="name">What is your email address? *</label>
						<input type="text" maxlength="50" name="customer-name" id="name" placeholder="We will keep it safe!" onfocus="this.placeholder=''">
					</div>

					<div class="form-input " style="height:182px;">
						<label for="name" style="margin-bottom:0; height: 18%;">We are all ears! *</label>
						<textarea type="text" maxlength="255" name="customer-name" id="name" value="Type your name here" onfocus="this.placeholder=''" placeholder="Share with us any information that might help us to respond you."></textarea>
					</div>

					<div class="form-input " style="height:91px;">
						<div class="row contact-submit">
							<button type="submit" class="btn contact shadow-sm mb-5" >Send message</button>
						</div>
					</div>
				</form>

				<div class="col-5">
					<div class="contact-right">
						<p style="margin-bottom: 6px;text-align:left; font-size: 1.1em; color:#425B6F;">Come in for a coffee <i style ="color:black;" class="fas fa-coffee"></i></p>
					</div>
					<div class="contact-right map">
						<img src="./img/map.PNG" alt="Map">
					</div>
					<div class="contact-right info">
						<p><i class="fas fa-map-marker-alt"></i> Lindstedtsvägen 24, 4th floor, 114 28 Stockholm, Sweden</p>
						<p style=""><i class="fas fa-envelope"></i>rami@globuzzer.com</p>
						<p><i class="fas fa-phone"></i>+46 73 555 5 134</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- footer -->

	<div class="footer">
		<div class="container row">
			<div class="col-6" style="padding-left: 40px;">
				<div class="footer-option">
				<a href="#">Home</a>
				<a href="#">Privacy</a>
				<a href="#">Terms & Conditions</a>
				<a href="#">Contact us</a>
				</div>
				<p class="copyright">&copy; Copyright statement</p>
			</div>
			<div class="col-6">
				<div class="social-media">
					<a href="#"><i class="fas fa-globe-europe"></i></a>
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-pinterest"></i></a>
					<a href="#"><i class="fab fa-linkedin-in"></i></a>
				</div>	
			</div>			
		</div>
	</div>

	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	 crossorigin="anonymous"></script>
	<script src="./js/index.js" type="text/javascript"></script>
</body>

</html>