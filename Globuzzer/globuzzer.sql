DROP DATABASE IF EXISTS globuzzer;

CREATE DATABASE globuzzer;

USE globuzzer;

CREATE TABLE `countries` (
    `countryID` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `image_path` varchar(255),
    `video_path` varchar(255),
    `video_thumbnail` varchar(255),
    PRIMARY KEY (countryID)
);

INSERT INTO `countries` (`countryID`, `title`, `image_path`, `video_path`, `video_thumbnail`) VALUES (NULL, 'Sweden', './img/sweden.jpg', './video/Sweden.mp4', './img/sweden-thumbnail.PNG' );
INSERT INTO `countries` (`countryID`, `title`, `image_path`, `video_path`, `video_thumbnail`) VALUES (NULL, 'Finland', './img/finland.jpg', './video/Finland.mp4', './img/finland-thumbnail.PNG');
INSERT INTO `countries` (`countryID`, `title`, `image_path`, `video_path`, `video_thumbnail`) VALUES (NULL, 'Norway', './img/norway1.jpg', './video/Norway.mp4', './img/norway-thumbnail.PNG');
INSERT INTO `countries` (`countryID`, `title`, `image_path`, `video_path`, `video_thumbnail`) VALUES (NULL, 'Denmark', './img/denmark.jpg', './video/Denmark.mp4', './img/denmark-thumbnail.PNG');