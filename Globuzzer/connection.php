<?php

  // address, username, pass, database
  $connection = mysqli_connect("localhost", "globuzzer", "", "globuzzer");

  // check connection
  if (!$connection) {
    die("<p style='text-align: center; margin-top: 60px; font-size: 14px; font-weight: bold; font-family: Lato, sans-serif; color: hsl(0, 0%, 40%);'>Failed to connect to the database.</p>"); 
  }
